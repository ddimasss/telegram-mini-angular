import { Component } from '@angular/core';
import { Button } from 'primeng/button';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'telegram-mini';
  msg = '';

  buttonClick() {
    this.msg = 'Click!';
  }
}
