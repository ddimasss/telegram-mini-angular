import { Routes } from '@angular/router';
import { TestComponent } from './test/test.component'

const routeConfig: Routes = [
    {
      path: 'test',
      component: TestComponent,
      title: 'Home details'
    }
  ];
  
  export default routeConfig;